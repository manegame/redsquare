const defaultTheme = require('tailwindcss/defaultTheme')
const fontFamily = {
	...defaultTheme.fontFamily,
	sans: ['system-ui', 'Open Sans', 'sans-serif'],
}

const generateZIndex = () => {
  const result = {}
  for (let i = 1; i <= 100; i++) { result[i] = i }
  return result
}

const zIndex = generateZIndex()

module.exports = {
	mode: 'jit',
	content: ['./src/**/*.{html,js,svelte,ts}'],
	darkMode: 'media', // or 'media' or 'class'
	theme: {
		fontFamily,
		extend: {
			colors: {
			  'lines': 'rgb(30 30 30)',
			  'fonts': 'rgb(30 30 30)',
			  'fonts-invert': 'rgb(255 255 255)',
			  'background': 'rgb(255 255 255)',
			  'yellow': 'rgb(255 255 0)',
			  'red': 'rgb(255 0 0)',
			  'green': 'rgb(0 255 0)',
        silver: '#bfbfbf'
			},
			margin: {
				'1px': '1px',
				'2px': '2px',
			},
			height: {
				'100vw': '100vw',
				'90vw': '90vw',
				'80vw': '80vw',
				'70vw': '70vw',
				'60vw': '60vw',
				'50vw': '50vw',
				'40vw': '40vw',
				'30vw': '30vw',
				'20vw': '20vw',
				'10vw': '10vw',
				'screen+1': 'calc(100vh + 1px)' // This can be used if you want to get the scrolling direction but the page is fullscreen
			},
      borderWidth: {
        '6': '6px'
      },
      zIndex
		}
	},
	variants: {
		extend: {},
	},
	plugins: [
	]
}